class Field {
    constructor(x,y,div,nextmas){
        this.posX=x;
        this.posY=y;
        this.checked = false;
        this.openned = false;
        this.div = div;
        this.mine=false;
        this.clear=false;
        this.number=false;
        this.next = nextmas;
    }
    switchCheck(){
        if(!this.openned){
            this.checked=!this.checked;
        }
    }
    open(){
        if(!this.openned){
            this.openned = true;
            this.div //delete class hide
        }
    }
    openNext(){
        if(this.clear&&!this.openned){
            this.open();
            this.next.forEach((element)=>{
                element.openNext();
            })
        }
    }

}


class Mine extends Field{
    constructor(x,y){
        super(x,y);
        this.mine=true;
        this.clear=false;
        this.number=false;
    }
}


class Clear extends Field{
    constructor(x,y){
        super(x,y);
        this.mine=false;
        this.clear=true;
        this.number=false;
    }
}


class Number extends Field{
    constructor(x,y){
        super(x,y);
        this.mine=false;
        this.clear=false;
        this.number=true;
    }
}